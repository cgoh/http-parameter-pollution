package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.util.UriComponentsBuilder;
import app.exception.BadRequestException;

import static app.PaymentAction.TRANSFER;
import static app.PaymentAction.WITHDRAW;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        try {
             PaymentAction action = PaymentAction.valueOf(request.getParameter("action"));
            Integer amount = Integer.parseInt(request.getParameter("amount"));
            if (action.equals(TRANSFER)) {
                System.out.println("Verify Controller: Going to transfer $"+amount);
                RestTemplate restTemplate = new RestTemplate();
                String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
                UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(fakePaymentUrl)
                    .queryParam("action", action)
                    .queryParam("amount", amount);
                ResponseEntity<String> response = restTemplate.getForEntity(
                        builder.toUriString(), String.class);
                return response.getBody();
            } else if (action.equals(WITHDRAW)) {
                return "Verify Controller: Sorry, you can only make transfer";
            } else {
                return "Verify Controller: You must specify action: transfer or withdraw";
            }
        } catch(RuntimeException ex) {
            throw new BadRequestException();
        }
    }

}
