package app;

import javax.validation.constraints.NotNull;

public final class Amount {

    @NotNull
    private final int amount;

    public Amount(String amount){
        this.amount = Integer.parseInt(amount);
    }
}
