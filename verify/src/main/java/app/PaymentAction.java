package app;

public enum PaymentAction {
    TRANSFER,
    WITHDRAW
}
